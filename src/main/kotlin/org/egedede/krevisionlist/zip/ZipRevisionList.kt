package org.egedede.krevisionlist.zip

import java.io.FileOutputStream
import java.nio.file.*
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

/**
 * Revision list backed by a zip archive
 */
class ZipRevisionList(path: String) {

    private val backend : Path = Path.of(path)

    /**
     * Adds a new list in the repository
     */
    fun newList(listPath: String, content: String, tags: String) {
        val zipFs = getFileSystem()
        zipFs.use { it: FileSystem ->
            write("""/$listPath/content.txt""", content.replace(",", "\n"), it)
            write("""/$listPath/metadata.txt""", """tags=$tags""", it)
        }

    }

    private fun write(path: String, content: String, fs: FileSystem?) {
        val javaPath = fs!!.getPath(path)
        Files.createDirectories(javaPath.parent)
        Files.write(javaPath, content.toByteArray(), StandardOpenOption.CREATE_NEW)
    }

    /**
     * Add a new item in a list
     */
    fun addItem(path: String, newValue: String) {
        val zipFs = getFileSystem()
        zipFs.use {
            val javaPath = zipFs!!.getPath("$path/content.txt")
            val oldContent = Files.readString(javaPath)
            Files.delete(javaPath)
            Files.write(javaPath, (oldContent+"\n"+newValue).toByteArray(), StandardOpenOption.CREATE_NEW)
        }
    }

    fun list(path: String) : Array<String> {
        val result = mutableListOf<String>()
        val zipFs = getFileSystem()
        zipFs.use {
            val javaPath = zipFs!!.getPath("""/$path""")
            Files.walkFileTree(javaPath, ContentReaderFileVisitor(result))
        }
        return result.toTypedArray()
    }

    private fun getFileSystem() = FileSystems.newFileSystem(backend, ClassLoader.getPlatformClassLoader())

    companion object {
        /**
         * Used to instantiate a new zip repository
         */
        fun initArchive(path: String, listName: String) {
            ZipOutputStream(FileOutputStream(path)).use {
                it.putNextEntry(ZipEntry("metadata.txt"))
                it.write("""name=$listName""".toByteArray())
                it.closeEntry()
            }
        }
    }
}