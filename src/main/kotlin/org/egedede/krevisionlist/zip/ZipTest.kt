package org.egedede.krevisionlist.zip

import java.nio.file.Files
import java.nio.file.Path

fun main(args: Array<String>) {
    val archivePath = "/tmp/testArchive.zip"
    if(Files.exists(Path.of(archivePath))) {
        Files.delete(Path.of(archivePath))
    }
    ZipRevisionList.initArchive(archivePath,"hello")
    val repository = ZipRevisionList(archivePath)
    repository.newList("mainList1/subList1","Péremptoire,Abscons,Moyen-Âge,Pourriture Communiste, Lumineux, Lumineuse","CE2")
    repository.addItem("mainList1/subList1","Gentille")
    repository.newList("mainList1/subList2","subList2 item","CE2")

    var list = repository.list("mainList1")
    list.forEach { println(it) }

    println("Write just sublist 1 now")
    list = repository.list("mainList1/subList1")
    list.forEach { println(it) }
}